#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Color

echo -e "${BLUE}Dowloading IPS for Linux amd 64...${NC}"
#if you use other system, try looking for it here:
#https://dist.ipfs.io/#go-ipfs

wget "https://dist.ipfs.io/go-ipfs/v0.4.21/go-ipfs_v0.4.21_linux-amd64.tar.gz"

echo -e "Done. ${PURPLE}Trying the init...${NC}"
ipfs init

echo -e "If you don't see your IPFS peer identity, something went wrong, check a different installation method. More info in the WIki of the project."

# aaah sick, check this https://medium.com/textileio/how-ipfs-peer-nodes-identify-each-other-on-the-distributed-web-8b5b6476aa5e
