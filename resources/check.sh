#!/bin/bash

function check_user {
  #check how many users are logged on and save a rewritable log
  w -s -h > log_users

  #save the amount of users that are connected in the system in COM
  COM=`wc -l log_users | awk '{print $1;}'`

  #if more than you, then alarm. you can change "1" if there are more users allowed
  if [ "$COM" != "1" ]
    then
      return 1
  else
    return 0
  fi
}

function user_watcher {
  check_user
  #gets check_user exit code
  if [ "$?" != "0" ]
    then
      #using notification
      notify-send Alert -t 10 "Someone's in your system!"
  else
    #echo "there's no one else connected"
    notify-send Watcher -t 10 "Everything under control."
  fi
}

#check each 10 seconds
while [ 1 ]; do
  user_watcher
  sleep 10
done
